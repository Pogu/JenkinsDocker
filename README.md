# JenkinsDocker

Build container:

	docker build -t myjenkins .

Create empty folder c:/JenkinsCache

Run Docker Container.

    docker run -p 8080:8080 -p 50000:50000 -v c:/JenkinsCache/:/var/jenkins_home myjenkins

Documentation of base image:

	https://github.com/jenkinsci/docker
