FROM jenkins/jenkins

USER root

RUN apt-get update && apt-get install -y python python-pip sloccount
RUN pip install coverage nose pylint pyinstaller

COPY plugins.txt /usr/share/jenkins/plugins.txt
RUN /usr/local/bin/plugins.sh /usr/share/jenkins/plugins.txt
