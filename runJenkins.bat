@echo off
set JCF="C:\JenkinsCache" 
echo "Check if jenkins Cache folder exist...-  %JCF%  "
IF EXIST %JCF%  (
    echo ".. Folder already exists. Will use it for Jenkinscache"
    ) ELSE (
    echo ".. Folder doesn't exist. Will create it for you. %JCF%"
      mkdir %JCF% )

  docker run -p 8080:8080 -p 50000:50000 -v c:/JenkinsCache/:/var/jenkins_home myjenkins
